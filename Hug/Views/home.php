<?php
require 'layout/header.php';
?>
    <div class="card mt-2 p-3" id="loading" style="display: block;">
        <div class="text-center">
            <h4>Loading...</h4>
            <img src="assets/img/loading.gif" style="width: 64px;" class="img-responsive">
        </div>
    </div>

    <div class="card mt-2">
        <div class="card-header">
            <h2>Previously detected pictures</h2>
        </div>
        <div class="card-body" id="prevImages">
            <?php
            foreach($images as $image) {
            ?>
                <div class="row mt-5 prevImage" id="<?=$image->id;?>">
                    <div class="col-md-12">
                        <h3 class="url"><?=$image->url;?></h3>
                    </div>
                    <div class="col-md-6 text-center">
                        <h5>original Image</h5>
                        <br>
                        <img class="org-image" src="<?=$image->path;?>">
                    </div>
                    <div class="col-md-6 text-center">
                        <h5>Face detected</h5>
                        <br>
                        <?=(($image->face)?'<img src="'.$image->face.'">':'No faces detected');?>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>

    <div id="imageRow" style="display: none">
        <div class="row mt-5 prevImage">
            <div class="col-md-12">
                <h3 class="url"></h3>
            </div>
            <div class="col-md-6 text-center">
                <h5>original Image</h5>
                <br>
                <img style="width: 300px" class="org-image" src="">
            </div>
            <div class="col-md-6 text-center">
                <h5>Face detected</h5>
                <br>
                <img  class="face-image" src="">
            </div>
        </div>
    </div>

    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script>
        $(function(){
            var images = [
                'http://serveruat.com/tests/sbe-resources/1.jpg',
                'http://serveruat.com/tests/sbe-resources/2.jpg',
                'http://serveruat.com/tests/sbe-resources/3.jpeg',
                'http://serveruat.com/tests/sbe-resources/4.png',
                'http://serveruat.com/tests/sbe-resources/5.bmp',
                'http://serveruat.com/tests/sbe-resources/6.jpg',
            ];
            images.forEach(function(url,index){
                $.post('./detect-image',{url:url},function(data){
                    if(data.image) {
                        if($('div#'+data.image.id).length == 0) {
                            $NewRow = $('#imageRow').clone();
                            $($NewRow).find('.prevImage').attr('id', data.image.id);
                            $($NewRow).find('.url').text(data.image.url);
                            $($NewRow).find('.org-image').attr('src', data.image.path);
                            if (data.image.face) {
                                $($NewRow).find('.face-image').attr('src', data.image.face);
                            } else {
                                $($NewRow).find('.face-image').parent('div').text('No faces detected');
                            }
                            $('#prevImages').append($($NewRow).html());
                        }
                    }
                    if(index == 5)
                        $('#loading').css('display','none');
                },'json');

            });
        });
    </script>

<?php
    require 'layout/footer.php';
?>