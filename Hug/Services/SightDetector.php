<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/24/18 3:40 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */


namespace Hug\Services;


class SightDetector
{
    public $username;
    public $password;
    public function __construct()
    {
        $this->username = getenv('sightengine_username');
        $this->password = getenv('sightengine_password');
    }

    /*
     * Detect faces
     */
    protected function CallFaceDetectorService($url){
        $client = new \Sightengine\SightengineClient($this->username, $this->password);
        return $client->check(['faces'])->set_url($url);
    }

    public function Detect($url,$width,$height){
        $ApiCall = $this->CallFaceDetectorService($url);
        if(!$ApiCall->status)
            return ['status'=>false,'msg'=>'Service provider fails'];

        $faces = [];
        if(count($ApiCall->faces)){
            foreach($ApiCall->faces as $face){
                if(!isset($face->x1))
                    continue;
                $faces[] = [
                    'width'         => $this->CalcDim($width,$face->x1,$face->x2),
                    'height'        => $this->CalcDim($height,$face->y1,$face->y2),
                    'x'             => intval($width * $face->x1),
                    'y'             => intval($height * $face->y1),
                ];
            }
        }
        return $faces;
    }


    protected function CalcDim($OriginalDim,$val1,$val2){
        return intval(abs(($OriginalDim * $val1) - ($OriginalDim * $val2)));
    }
}