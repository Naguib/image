<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/19/18 1:49 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

namespace Hug;

use Illuminate\Database\Capsule\Manager as Capsule;

class Migrations
{
    public static function postInstall(){
        require __DIR__.'/../bootstrap.php';
        /*
         * Create directories
         */
        if(!is_dir(app_base('../public/original'))){
            mkdir(app_base('../public/original'), 777, true);
        }

        if(!is_dir(app_base('../public/original/resized'))){
            mkdir(app_base('../public/original/resized'), 777, true);
        }
        if(!is_dir(app_base('../public/original/faces'))){
            mkdir(app_base('../public/original/faces'), 777, true);
        }
        if(!is_dir(app_base('../database'))){
            mkdir(app_base('../database'), 777, true);
        }
        //Create sqlite DB file
        if(getenv('Database_type') == 'sqlite') {
            if (!file_exists(app_base('../database/db.sqlite'))) {
                touch(app_base('../database/db.sqlite'));
            }
        }

        /*
         * Create Images table
         */
        if(!Capsule::schema()->hasTable('images')) {
            Capsule::schema()->create('images', function ($table) {
                $table->increments('id');
                $table->string('url');
                $table->string('size');
                $table->string('path')->nullable();
                $table->string('data')->nullable();
                $table->string('face')->nullable();
                $table->timestamps();
            });
        }
    }

}
