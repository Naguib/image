<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/18/18 11:55 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */
if (! function_exists('view')) {
    function view($file,$data) {
        extract($data);
        require app_base('Views/' . $file);
        exit;
    }
}

if (! function_exists('app_base')) {
    function app_base($path = null)
    {
        return __DIR__ . '/../' . $path;
    }
}

if (! function_exists('dd')) {
    function dd(...$data){
        die(var_dump($data));
    }
}

