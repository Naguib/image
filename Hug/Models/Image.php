<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/19/18 1:37 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

namespace Hug\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Image extends Eloquent
{
    protected $table = 'images';
    protected $fillable = ['url','size','path','data','face'];


}