<?php
require __DIR__.'/../../bootstrap.php';

$url = $_POST['url'];
$image = \Hug\Models\Image::where('url', $url)->first();
if (!$image) {
    try {
        $img = \Intervention\Image\ImageManagerStatic::make($url);
        /**
         * Save original sized
         */
        $UniqueImageName = uniqid() . '.jpg';
        $img->save(app_base('../public/original/' . $UniqueImageName));
        $SaveImage = \Hug\Models\Image::create([
            'url' => $url,
            'size' => $img->filesize(),
            'path' => 'original/resized/' . $UniqueImageName,
        ]);

        /**
         * Save resized Image
         */
        $img->resize(getenv('resize_width'), getenv('resize_height'))
            ->save(app_base('../public/original/resized/' . $UniqueImageName));
        /**
         * Detect faces
         */
        $SightDetector = new \Hug\Services\SightDetector();

        $facesDetected = $SightDetector->Detect($url, $img->width(), $img->height());
        //Only one face detect
        $face = end($facesDetected);
        if ($face) {
            $img->crop($face['width'], $face['height'], $face['x'], $face['y'])
                ->resize(getenv('resize_width'), getenv('resize_height'))
                ->save(app_base('../public/original/faces/' . $UniqueImageName));
            $SaveImage->update([
                'data' => json_encode($face),
                'face' => 'original/faces/' . $UniqueImageName,
            ]);
        }
        $image = $SaveImage;
    } catch (Exception $e){
        echo json_encode([
            'status'=>true,
            'error'=>$e->getCode().': '.$e->getMessage().', file:'.$e->getFile().'('.$e->getLine().')',
            'trace'=>$e->getTraceAsString(),
        ]);
        die;
    }
}

echo json_encode([
    'status'=>true,
    'image'=>$image
]);