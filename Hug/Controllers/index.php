<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/21/18 9:08 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

$data['title'] = 'Home page';
$data['images'] = \Hug\Models\Image::all();
view('home.php',$data);