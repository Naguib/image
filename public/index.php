<?php
require __DIR__.'/../bootstrap.php';

/**
 * Register routes
 */
$routes = [
    ''                  =>      'Controllers/index.php',
    'detect-image'      =>      'Controllers/detect.php',
];

/**
 * Extract URI
 */
$uri =explode('/',trim($_SERVER['REQUEST_URI'],'/'));
$uri = end($uri);
if(strpos($uri,'?') !== false){
    $uri = explode('?',$uri)[0];
}
if($uri == getenv('dir_name')){
    $uri='';
}

/**
 * Route to controller
 */
if(array_key_exists($uri,$routes)){
    require app_base($routes[$uri]);
} else {
    require app_base('Controllers/404.php');
}