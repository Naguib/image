Installation
-------------
Just do a "composer install" and everything should be ready.
 
 #### App steps:
 
 - Download image to public/original
    
    - Save URL image data to DB
 
 - Save reszied image to public/resized
 
 - Detect face and save image to public/faces
 
    - Save face data to DB
  
 #### Few notes:

 - Queued by JS 
 
 - DB used Sqlite OR mysql can be changed on .env file.

 - Specify width and height for the resized image in .env file default 250,400

 - Face detection lib used is <a href="https://sightengine.com">Sightengine</a>
 
 #### Docker:
 
 for using docker image.
 
  - Build the image
    
    - <code>docker build -t mostafanaguib/php-native .</code>
    
  - Run container
  
    - <code>docker run --rm -d -p 8080:80 mostafanaguib/php-native</code>
    
    server should be up on <code>localhost:8080</code>