FROM composer:1.8 as composer-builder
WORKDIR /app
COPY composer.json .
COPY composer.lock .
COPY . .
RUN composer install

FROM php:7.2-apache
RUN apt-get update && apt-get install sqlite3 libsqlite3-dev libpng-dev -y && a2enmod rewrite && docker-php-ext-install gd && docker-php-ext-install pdo_mysql
WORKDIR /var/www
COPY . .
COPY --from=composer-builder /app html
COPY public/ html
RUN mkdir -p html/public/original/faces
RUN mkdir -p html/public/original/resized