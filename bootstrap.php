<?php
/**
 * @Author Mostafa Naguib.
 * @Copyright Maximum Develop
 * @FileCreated: 12/19/18 2:43 PM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */
require 'vendor/autoload.php';
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');
require app_base('Helpers/Database.php');